# Drawing 7 x 7 collage
import matplotlib.pyplot as plt
import pandas as pd
import cv2
from os.path import join, exists
from random import randint
from os import mkdir

name = 'pixdeluxe'
rows = 7
cols = 7
csv_dir = '/home/ms_lilibeth/crawler_data/clasterization/DPGMM_50_5000'
img_dir = '/home/ms_lilibeth/crawler_data/images'
save_to = join(csv_dir, name)
df = pd.read_csv(join(csv_dir, name + '.components.csv'))


c_num = max(df['label']) + 1
if not exists(save_to):
    mkdir(save_to)
for c in range(0, c_num):
    fig, ax = plt.subplots()
    df_tmp = df.loc[df['label'] == c]
    if df_tmp.shape[0] < rows*cols:
        rng = df_tmp.shape[0]
    else:
        rng = rows*cols
    index_used = []  # берем случайные фото из кластера
    plt.subplots_adjust(left=None, right=None, bottom=None, top=None, wspace=None, hspace=None)
    for i in range(rng):
        if rng > rows*cols:
            index = randint(0, df_tmp.shape[0]-1)
            while index in index_used:
                index = randint(0, df_tmp.shape[0]-1)
            index_used.append(index)
        else:
            index = i

        img_path = join(img_dir, name, str(df_tmp.iloc[index]['gm_id']) + '.jpg')
        if not exists(img_path):
            print(name, ': Img path does not exist (%s)' % img_path)
            img_path = img_dir + 'img_not_found.png'
        try:
            img = plt.imread(img_path)
        except IOError:
            print('cannot identify image file: %s' % img_path)
            img_path = img_dir + 'img_not_found.png'
        # b, g, r = cv2.split(img)  # get b,g,r
        # img = cv2.merge([r, g, b])  # switch it to rgb
        # mx = min(80 / img.shape[0], 80 / img.shape[1])
        # img = cv2.resize(img, (0, 0), img, mx, mx)
        plt.subplot(rows, cols, i+1, frameon=False, xticks=[], yticks=[])
        plt.imshow(img)
    # plt.show()

    fig_name = name + '.' + str(c) + '.jpg'
    fig.savefig(join(save_to, fig_name), figsize=(3, 3), dpi=300)






