import pandas as pd
from sklearn.mixture import DPGMM
import os.path
import json
from os.path import join, exists
import numpy as np
import statistics
import time
from datetime import datetime
from math import log2
import sys
num_file_items = 1024*4
desc_size = 2048

# desc_path = '/home/ms_lilibeth/crawler_data/descriptors/pixdeluxe'
# csv_path = '/home/ms_lilibeth/crawler_data/rest_info/pixdeluxe.csv'
# limit = 30000
limit_date = datetime.strptime("2012-01-01", "%Y-%m-%d")
limit_date = time.mktime(limit_date.timetuple())
n_components_max = 50
# alpha = 3
if len(sys.argv) <= 1:
    print('No arguments')
    exit()
if len(sys.argv) == 2:
    join_csvs = False
    name = sys.argv[1]
    print(name)
if len(sys.argv) > 2:
    join_csvs = True
    names = [name for name in sys.argv[1:]]
    print(names)

if join_csvs:
    desc_dir = '/home/traineeship/portfolio'
    csv_dir = '/home/traineeship/portfolio'
else:
    desc_path = join('/home/traineeship/portfolio', name, 'descriptors')
    csv_path = join('/home/traineeship/portfolio', name, 'rest_info.csv')
    # desc_path = '/home/ms_lilibeth/crawler_data/descriptors/gradyreese'
    # csv_path = '/home/ms_lilibeth/crawler_data/rest_info/gradyreese.csv'


def get_descriptor(desc_dir, i):
    offset = i % num_file_items
    file_id = i / num_file_items
    path = join(desc_dir, "descriptors_inception_v3_pool3_%d.bin" % file_id)
    if not exists(path):
        msg = 'PATH TO DESCRIPTORS DOES NOT EXIST: %s' % path
        raise BaseException(msg)
    f = open(path, "rb")
    f.seek(8 * 2048 * offset, os.SEEK_SET)
    desc = np.fromfile(f, dtype=np.double, count=2048)
    f.close()
    return desc

X = []

if join_csvs:
    authors = []
    for name in names:
        csv_path = join(csv_dir, name, 'rest_info.csv')
        desc_path = join(desc_dir, name, 'descriptors')

        if not exists(csv_path):
            msg = ' %s: PATH TO CSV FILE DOES NOT EXIST: %s' % (name, csv_path)
            raise BaseException(msg)
        if not exists(join(desc_path, 'descriptors_map.json')):
            msg = '%s : PATH TO DESCRIPTORS DOES NOT EXIST: %s' % (name, desc_path)
            raise BaseException(msg)

        df = pd.read_csv(csv_path)
        df = df.loc[df['date'] > limit_date]

        with open(join(desc_path, 'descriptors_map.json'), 'r') as f:
            id_map = json.load(f)  # dict: {'gm_id': desc_id, ...}

        ids = df['gm_id'].tolist()
        downloads = df['downloads'].tolist()
        not_mapped = []
        for id in ids:
            try:
                X.append(get_descriptor(desc_path, id_map[str(id)]))
            except KeyError:
                not_mapped.append(id)
                pass

        indices_tmp = [ids.index(i) for i in not_mapped]
        ids_tmp = [id for id in ids if id not in not_mapped]
        ids += ids_tmp
        downloads += [d for d in downloads if downloads.index(d) not in indices_tmp]
        authors += [name for i in range(len(ids_tmp))]
        print(name, ' : ', len(ids_tmp), ' works')

else:
    if not exists(csv_path):
        print('PATH TO CSV FILE DOES NOT EXIST: ', csv_path)
        raise BaseException
    if not exists(join(desc_path, 'descriptors_map.json')):
        print('PATH TO DESCRIPTORS DOES NOT EXIST: ', desc_path)
        raise BaseException
    with open(join(desc_path, 'descriptors_map.json'), 'r') as f:
        id_map = json.load(f)  # dict: {'gm_id': desc_id, ...}
    df = pd.read_csv(csv_path)
    df = df.loc[df['date'] > limit_date]
    ids = df['gm_id'].tolist()
    downloads = df['downloads'].tolist()
    not_mapped = []
    for id in ids:
        try:
            X.append(get_descriptor(desc_path, id_map[str(id)]))
        except KeyError:
            not_mapped.append(id)
            pass

    indices_tmp = [ids.index(i) for i in not_mapped]

    for i in indices_tmp:
        del ids[i]
        del downloads[i]


X = np.array(X)
print('X shape: ', X.shape)


# df = df.sort_values(by='date', ascending=False)
# if limit < df.shape[0]:
#     df = df.loc[:limit-1]

alpha = n_components_max / log2(X.shape[0])
print('alpha: ', alpha)
clf = DPGMM(n_components_max, alpha=alpha)
print("Fit-predict...")
y = clf.fit_predict(X)
print("Saving...")
c_num = max(y) + 1

if join_csvs:
    df = pd.DataFrame({'author': authors, 'gm_id': ids, 'label': y, 'downloads': downloads})
    df.to_csv('mix'+'.components.csv', index=False)
else:
    print('ids len: ', len(ids))
    print('y len: ', len(y))
    print('downloads len: ', len(downloads))
    df = pd.DataFrame({'gm_id': ids, 'label': y, 'downloads': downloads})
    df.to_csv(name + '.components.csv', index=False)
components = list(range(c_num))
downloads_mean = []
samples_num = []
for i in components:
    df_tmp = df.loc[df['label'] == i]
    print('comp ', i, ': samples_num: ', df_tmp.shape[0])
    if df_tmp.shape[0] == 0:
        downloads_mean.append(0)
    else:
        downloads_mean.append(statistics.mean(df_tmp['downloads']))
    samples_num.append(df_tmp.shape[0])

print('components num: ', len(components))
print('samples num: ', len(samples_num))
print('len downloads_mean: ', len(downloads_mean))
df = pd.DataFrame({'component': components, 'samples_num': samples_num, 'downloads_mean': downloads_mean})
df = df.sort_values(by='downloads_mean', ascending=False)
if join_csvs:
    df.to_csv('mix.components.stat.csv', index=False)
else:
    df.to_csv(name + '.components.stat.csv', index=False)




